$(window).scroll(function(){
   let  wintop = $(window).scrollTop(), 
        docheight = $(document).height(), 
        winheight = $(window).height();

   let scrolled = (wintop/(docheight - winheight))*100;

   $('.scroll_line').css('width', (scrolled + '%') )
})